#include <QCoreApplication>
#include <QRegularExpression>
#include <QVector>
#include <QStack>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <regex>
#include <map>
#include <utility>


enum class TokenKind {
        FOR     ,
        WHILE   ,
        IF      ,
        ELSE    ,
        INCR    ,
        FUNC    ,
        STR     ,
        DECR    ,
        L_RBR   ,//(
        R_RBR   ,//)
        L_SBR   ,//[
        R_SBR   ,//]
        L_BR    ,  //{
        R_BR    ,//}
        SMCLN   ,//;
        DDOT    ,//:
        DOT     ,//.
        COMM    ,//,
        OP      ,
        NEW     ,
        LIST    ,
        TABLE   ,
        VAR     ,
        ASSIGN_OP ,
        NUMB    ,
        SPACE   ,
};

#define __OUT_ENUM(kind) case TokenKind::kind: { return out << "TKN:" << #kind; }
std::ostream& operator<<(std::ostream& out, const TokenKind kind){
    switch(kind) {
        __OUT_ENUM(FOR);
        __OUT_ENUM(WHILE);
        __OUT_ENUM(IF);
        __OUT_ENUM(ELSE);
        __OUT_ENUM(INCR);
        __OUT_ENUM(DECR);
        __OUT_ENUM(FUNC);
        __OUT_ENUM(STR);
        __OUT_ENUM(L_RBR);
        __OUT_ENUM(R_RBR);
        __OUT_ENUM(L_SBR);
        __OUT_ENUM(R_SBR);
        __OUT_ENUM(L_BR);
        __OUT_ENUM(R_BR);
        __OUT_ENUM(SMCLN);
        __OUT_ENUM(DDOT);
        __OUT_ENUM(DOT);
        __OUT_ENUM(COMM);
        __OUT_ENUM(OP);
        __OUT_ENUM(LIST);
        __OUT_ENUM(TABLE);
        __OUT_ENUM(NEW);
        __OUT_ENUM(VAR)
        __OUT_ENUM(ASSIGN_OP);
        __OUT_ENUM(NUMB);
        __OUT_ENUM(SPACE);
    }
}

enum class AstKind {
        lang,
        expr,
        stmt,
        while_expr,
        while_body,
        while_head,
        if_expr,
        if_body,
        if_head,
        value,
        assign_expr,
        else_body,
        stmt_list,
        OP,
        func_expr,
        new_expr ,
        LIST,
        TABLE,
        FUNC,
        ARGS,
        VAR,
        ASSIGN_OP ,
        NUMB ,
        STR ,
        L_RBR,
        R_RBR,
        FALSE_JMP,
        JMP
};

#define __OUT_ENUM(kind) case AstKind::kind: { return out  << #kind; }
std::ostream& operator<<(std::ostream& out, const AstKind kind){
    switch(kind) {
        __OUT_ENUM(lang);
        __OUT_ENUM(expr);
        __OUT_ENUM(stmt);
        __OUT_ENUM(while_expr);
        __OUT_ENUM(while_body);
        __OUT_ENUM(while_head);
        __OUT_ENUM(else_body);
        __OUT_ENUM(if_expr);
        __OUT_ENUM(if_body);
        __OUT_ENUM(if_head);
        __OUT_ENUM(value);
        __OUT_ENUM(assign_expr);
        __OUT_ENUM(OP);
        __OUT_ENUM(LIST);
        __OUT_ENUM(TABLE);
        __OUT_ENUM(func_expr);
        __OUT_ENUM(new_expr);
        __OUT_ENUM(VAR)
        __OUT_ENUM(ASSIGN_OP);
        __OUT_ENUM(NUMB);
        __OUT_ENUM(STR);
        __OUT_ENUM(FUNC);
        __OUT_ENUM(ARGS);
    }
}

enum class TypeKind {
        INT,
        FLOAT,
        CHAR,
        BOOL,
        STRING,
        ADDR,
        LINKED_LIST,
        HASH_TABLE,
};

#define __OUT_ENUM(kind) case TypeKind::kind: { return out  << #kind; }
std::ostream& operator<<(std::ostream& out, const TypeKind kind){
    switch(kind) {
        __OUT_ENUM(INT);
        __OUT_ENUM(FLOAT);
        __OUT_ENUM(CHAR);
        __OUT_ENUM(BOOL);
        __OUT_ENUM(STRING);
        __OUT_ENUM(ADDR);
        __OUT_ENUM(LINKED_LIST);
        __OUT_ENUM(HASH_TABLE);
    }
}

enum class OpKind {
        OP,
        VAR,
        FUNCTION,
        ASSIGN_OP ,
        NUMB ,
        STR ,
        FALSE_JMP,
        JMP,
        ADDR
};

#define __OUT_ENUM(kind) case OpKind::kind: { return out  << #kind; }
std::ostream& operator<<(std::ostream& out, const OpKind kind){
    switch(kind) {
        __OUT_ENUM(OP);
        __OUT_ENUM(VAR);
        __OUT_ENUM(FUNCTION);
        __OUT_ENUM(ASSIGN_OP);
        __OUT_ENUM(NUMB);
        __OUT_ENUM(STR);
        __OUT_ENUM(FALSE_JMP);
        __OUT_ENUM(JMP);
        __OUT_ENUM(ADDR);
    }
}

std::vector<std::pair<TokenKind,QRegularExpression>> Tokens {
    { TokenKind::FOR , QRegularExpression("for")},
    { TokenKind::WHILE , QRegularExpression("while")},
    { TokenKind::IF ,QRegularExpression("if")},
    { TokenKind::ELSE , QRegularExpression("else")},
    { TokenKind::NEW , QRegularExpression("new")},
    { TokenKind::LIST , QRegularExpression("List")},
    { TokenKind::TABLE , QRegularExpression("Table")},
    { TokenKind::FUNC , QRegularExpression("\\.\\w+")},
    { TokenKind::STR , QRegularExpression("\\'\\w+\\'")},
    { TokenKind::INCR , QRegularExpression("\\+\\+")},
    { TokenKind::DECR , QRegularExpression("\\-\\-")},
    { TokenKind::L_RBR , QRegularExpression("\\(")},
    { TokenKind::R_RBR , QRegularExpression("\\)")},
    { TokenKind::L_SBR , QRegularExpression("\\[")},
    { TokenKind::R_SBR , QRegularExpression("\\]")},
    { TokenKind::L_BR , QRegularExpression("\\{")},
    { TokenKind::R_BR , QRegularExpression("\\}")},
    { TokenKind::SMCLN , QRegularExpression("\\;")},
    { TokenKind::DOT , QRegularExpression("\\.")},
    { TokenKind::DDOT , QRegularExpression("\\:")},
    { TokenKind::COMM, QRegularExpression("\\,")},
    { TokenKind::OP , QRegularExpression("[+-/*<>!&|]+")},
    { TokenKind::VAR , QRegularExpression("[a-z]+")},
    { TokenKind::ASSIGN_OP ,QRegularExpression("=")},
    { TokenKind::NUMB , QRegularExpression("[0-9]+")},
    { TokenKind::SPACE, QRegularExpression("\\s+")}
};

struct LinkedList;
class HashTable;


struct Variable{
    QString name;
    TypeKind type;
    int int_value;
    int addr_value;
    float float_value;
    char char_value;
    QString str_value;
    bool bool_value;
    LinkedList* list_value;
    HashTable* table_value;

    Variable (TypeKind type, int val){
        this->type = type;
        if (type == TypeKind::ADDR){
            this->addr_value = val;
        }
        else{this->int_value = val;}
    }
    Variable (TypeKind type, float val){
        this->type = type;
        this->float_value = val;
    }
    Variable (TypeKind type, QString val){
        this->type = type;
        this->str_value = val;
    }
    Variable (TypeKind type, bool val){
        this->type = type;
        this->bool_value = val;
    }
    Variable (TypeKind type, char val){
        this->type = type;
        this->char_value = val;
    }
    Variable (TypeKind type, LinkedList* val){
        this->type = type;
        this->list_value = val;
    }
    Variable (TypeKind type, HashTable* val){
        this->type = type;
        this->table_value = val;
    }
    Variable (int val){
        this->int_value = val;
    };
    Variable (QString name){
        this->name = name;
    };
    Variable (Variable* data){
        this->type = data->type;
        switch (data->type) {
        case TypeKind::INT:{this->int_value = data->int_value;}break;
        case TypeKind::STRING:{this->str_value = data->str_value;}break;
        }
    }
};

struct LinkedList{
    Variable* data;
    LinkedList* next;
    LinkedList* prev;

    LinkedList(){
        data = nullptr;
        next = nullptr;
        prev = nullptr;
     }
     void append_data(Variable* data){
          auto obj = new LinkedList();
          Variable * new_data = new Variable(data);
          obj->data = new_data;

         auto ptr = this;
         while (ptr->next != nullptr) {
             ptr = ptr->next;
         }
          ptr->next = obj;
         obj->prev = ptr;
    }
    Variable* get_data(int index){
         auto ptr = this->next;
         int i = index-1;
          while (i >= 0){
              if (ptr->next != nullptr){
                   ptr = ptr->next;
                  i--;
              }
             else{break;}
        }
        return ptr->data;
    }
    void print_list(){
        auto ptr = this->next;
        int index = 0;
        while(ptr != nullptr){
            switch (ptr->data->type) {
            case TypeKind::INT:{std::cout<<"\t["<<index<<"]:"<<ptr->data->int_value<<"\n";}break;
            case TypeKind::STRING:{std::cout<<"\t["<<index<<"]:"<<ptr->data->str_value.toStdString()<<"\n";}break;
            }
            ptr = ptr->next;
            index++;
        }
    }

};

struct Pair{
    Variable* key;
    Variable* data;

    Pair(Variable* key, Variable* data){
        this->key = key;
        this->data = data;
    }
};

class HashTable {
public:
  struct Pair {
      Variable* key;
      Variable* value;
      Pair() = default;
  };

  struct Bucket {
      QVector<Pair> entries;
      Bucket() = default;
  };

  QVector<Bucket> bucket;

  HashTable() {
      elements_amount = 0;
      bucket          = QVector<Bucket>(4);
  };

  int elements_amount;

  int Hash(Variable* key);

  void insert(Variable* key, Variable* value, bool recount = true);

  Variable* get_table(Variable* key);

  void resize();

  void print_table(){
        for (const auto& b : bucket) {
            for (const auto& p : b.entries) {
                switch (p.key->type) {
                case TypeKind::INT:{std::cout<<"\t{"<<p.key->int_value<<" , ";}break;
                case TypeKind::STRING:{std::cout<<"\t{"<<p.key->str_value.toStdString()<<" , ";}break;
                }
                switch (p.value->type) {
                case TypeKind::INT:{std::cout<<p.value->int_value<<"}\n";}break;
                case TypeKind::STRING:{std::cout<<p.value->str_value.toStdString()<<"}\n";}break;
                }
            }
        }
  }
};

bool operator==(const Variable& a, const Variable& b) {
    bool result = a.type == b.type;
    if (result) {
        switch (a.type) {
            case TypeKind::INT: return a.int_value == b.int_value;
            case TypeKind::STRING: return a.str_value == b.str_value;
        }
    }
    return result;
}

void HashTable::insert(Variable* key, Variable* value, bool recount) {
    auto idx = key->int_value % this->bucket.size();
    if (elements_amount * 2 > this->bucket.size()) {
        resize();
    }
    if (bucket[idx].entries.size() == 0) {
        bucket[idx].entries.push_back({key, value});
        if (recount) {
            elements_amount++;
        }
    } else {
        for (int pair_idx = 0; pair_idx < this->bucket[idx].entries.size();
             ++pair_idx) {
            if (*bucket[idx].entries[pair_idx].key == *key) {
                bucket[idx].entries[pair_idx].value = value;
                return;
            }
        }
        bucket[idx].entries.push_back({key, value});
        if (recount) {
            elements_amount++;
        }
    }
}

Variable* HashTable::get_table(Variable* key) {
    auto hash = key->int_value;
    auto idx  = hash % this->bucket.size();
    for (int pair_idx = 0; pair_idx < this->bucket[idx].entries.size();
         ++pair_idx) {
        if (this->bucket[idx].entries.size() == 0) {
            std::cerr << "The bucket is empty" << std::endl;
            throw "Empty bucket";
        } else if (*this->bucket[idx].entries[pair_idx].key == *key) {
            return bucket[idx].entries[pair_idx].value;
        }
    }
    std::cerr << "No matching key "<< std::endl;
    abort();
}

void HashTable::resize() {
    auto old_bucket = this->bucket;
    bucket          = QVector<Bucket>(this->bucket.size() * 2);
    std::cout << "Resize to " << bucket.size() << " buckets " << std::endl;
    for (auto& old : old_bucket) {
        for (auto& e : old.entries) {
            this->insert(e.key, e.value, false);
        }
    }
}





struct Token{
    QString value;
    TokenKind type;
};

struct Operation{
    OpKind type;
    QString value;
};


QVector<Token>matches;
QVector<Variable>variables;
QVector<Token>::Iterator match_it;
QVector<Operation>poliz;
QStack<Variable*>stack;

int lexer(QString code){
    int startpos=0;

    while (startpos<code.length())
    {
        for (const auto& pattern:Tokens)
        {
            QRegularExpressionMatch match = pattern.second.match(code,startpos);

            if (match.hasMatch()){
                int pos = match.capturedStart(0);
                if (pos == startpos)
                {
                    QString found_piece = code.mid(startpos,match.capturedLength());
                    if (pattern.first != TokenKind::SPACE)
                        matches.push_back({found_piece, pattern.first});
                    startpos += match.capturedLength();
                    break;
                }
            }
        }
    }
    for (int k = 0; k < matches.size(); k++)
    {
        std::cout << "| " << matches[k].type << " |" << "    [" << matches[k].value.toStdString()  << "]" << std::endl;
    }
    return 0;
}

class code_obj{
public:
    code_obj(AstKind type, QString value){
        this->type = type;
        this->value = value;
    }
    code_obj(AstKind type){
        this->type = type;
        this->value = "";
    }

    QVector<code_obj*> get_childs(){
        return childs;
    }
    code_obj* get_parent(){
        return this->parent;
    }
    AstKind get_type(){
        return this->type;
    }
    QString get_value(){
        return this->value;
    }

    int get_prec(){
        if (this->value == "*")
            return 3;
        if (this->value == "/")
            return 3;
        if (this->value == "+")
            return 2;
        if (this->value == "-")
            return 2;
        if (this->value == "<")
            return 1;
        if (this->value == ">")
            return 1;
        return 0;
    }

    void set_child(code_obj* child_obj){
        childs.append(child_obj);
        child_obj->parent = this;
    }
    void set_parent(code_obj* parent){
        parent->set_child(this);
    }

    void print_tree(int level){
        std::cout <<level<<": ";
        for (int i=0; i<level; i++)
            std::cout <<"\t";
        std::cout << this->type;
        if (this ->value != "")
            std::cout <<"("<<this->value.toStdString()<<")";
        std::cout << "\n";
        if (!this->childs.isEmpty())
            for (it = childs.begin(); it != childs.end();++it){
                (*it)->print_tree(level+1);
            }
    }
private:
    AstKind type;
    QString value;
    code_obj* parent;
    QVector<code_obj*> childs;
    QVector<code_obj*>::Iterator it;
};

QVector<code_obj*>pnot_expr;
QStack<code_obj*>stack_expr;

void print_var(){
    std::cout<< "NAME\tTYPE\tVALUE\n";
    for (const auto& itr:variables) {
        std::cout<< itr.name.toStdString() << "\t" <<
                    itr.type<<"\t";
        switch (itr.type) {
        case TypeKind::INT:{
            std::cout << itr.int_value <<"\n";
        }break;
        case TypeKind::FLOAT:{
            std::cout << itr.float_value <<"\n";
        }break;
        case TypeKind::STRING:{
            std::cout << itr.str_value.toStdString() <<"\n";
        }break;
        case TypeKind::BOOL:{
            std::cout << itr.bool_value <<"\n";
        }break;
        case TypeKind::LINKED_LIST:{
            std::cout << "\n";
            itr.list_value->print_list();
        }break;
        case TypeKind::HASH_TABLE:{
            std::cout<< "\n";
            itr.table_value->print_table();
        }break;
        }
    }
}


void parse_value        (code_obj* value){
    code_obj* new_obj;
    //std::cout <<"parcing value\n";
    switch (match_it->type) {
    case TokenKind::VAR:{
        new_obj = new code_obj(AstKind::VAR, match_it->value);
        value->set_child(new_obj);
    }break;
    case TokenKind::NUMB:{
        new_obj = new code_obj(AstKind::NUMB, match_it->value);
        value->set_child(new_obj);
    }break;
    case TokenKind::STR:{
        new_obj = new code_obj(AstKind::STR, match_it->value.mid(1,match_it->value.length()-2));
        value->set_child(new_obj);
    }break;
    }
}

void parse_new_expr     (code_obj* new_expr){
    code_obj* new_obj;
    //std::cout <<"parcing new_expr\n";
    match_it++;
    switch (match_it->type) {
    case TokenKind::LIST:{
        new_obj = new code_obj(AstKind::LIST);
    }break;
    case TokenKind::TABLE:{
        new_obj = new code_obj(AstKind::TABLE);
    }break;
    }
    new_expr->set_child(new_obj);
    match_it++;
}

void parse_func_expr        (code_obj* func_expr);

void parse_expr         (code_obj* expr){
    code_obj* new_obj;
    //std::cout <<"parcing expr on :"<< match_it->type<<"\n";

    bool brk = false;

    while (match_it <= matches.end()) {
        switch (match_it->type) {
        case TokenKind::L_RBR:{
            new_obj = new code_obj(AstKind::L_RBR);
            stack_expr.push(new_obj);
        }break;
        case TokenKind::R_RBR:{
            while (stack_expr.top()->get_type() != AstKind::L_RBR) {
                pnot_expr.push_back(stack_expr.pop());
            }
            stack_expr.pop();
        }break;
        case TokenKind::OP:{
            new_obj = new code_obj(AstKind::OP, match_it->value);
            while ((!stack_expr.isEmpty())&&
                   (stack_expr.top()->get_prec()>new_obj->get_prec())) {
                pnot_expr.push_back(stack_expr.pop());
            }
            stack_expr.push(new_obj);
        }break;
        case TokenKind::STR:{
            new_obj = new code_obj(AstKind::value);
            parse_value(new_obj);
            pnot_expr.push_back(new_obj);
        }break;
        case TokenKind::NUMB:{
            new_obj = new code_obj(AstKind::value);
            parse_value(new_obj);
            pnot_expr.push_back(new_obj);
        }break;
        case TokenKind::VAR:{
            new_obj = new code_obj(AstKind::value);
            parse_value(new_obj);
            pnot_expr.push_back(new_obj);
        }break;
        case TokenKind::FUNC:{
            match_it--;
            new_obj = new code_obj(AstKind::func_expr);
            parse_func_expr(new_obj);
            pnot_expr.pop_back();
            pnot_expr.push_back(new_obj);
            match_it--;
            match_it--;
        }break;
        case TokenKind::SMCLN:{
            brk = true;
        }break;
        case TokenKind::L_BR:{
            brk = true;
        }break;
        case TokenKind::NEW:{
            new_obj = new code_obj(AstKind::new_expr);
            parse_new_expr(new_obj);
            expr->set_child(new_obj);
            return;
        }
        }

        if (brk)
            break;
        match_it++;

    }
    while (!stack_expr.isEmpty()){
        pnot_expr.push_back(stack_expr.pop());
    }
    for (int k = 0; k < pnot_expr.size(); k++)
    {
        std::cout << "|" << pnot_expr[k]->get_type() << "|" << "["
        << pnot_expr[k]->get_value().toStdString() << "]" << std::endl;
    }



    while (pnot_expr.size()!=0) {
        switch (pnot_expr[0]->get_type()) {
        case AstKind::value:{
            stack_expr.push(pnot_expr.takeFirst());
        }break;
        case AstKind::func_expr:{
            stack_expr.push(pnot_expr.takeFirst());
        }break;
        case AstKind::OP:{
            new_obj = new code_obj(AstKind::expr);
            new_obj->set_child(pnot_expr.takeFirst());
            new_obj->set_child(stack_expr.pop());
            new_obj->set_child(stack_expr.pop());
            stack_expr.push(new_obj);
        }
            break;

        }
    }
    *expr = *stack_expr.pop();

    //std::cout <<"parsinf expr has completed on:"<< match_it->type <<"\n";
}

void parse_stmt         (code_obj* stmt);


void parse_list(code_obj* list){
    code_obj* new_obj;
    //std::cout <<"parcing list\n";


}
void parse_while_body   (code_obj* while_body){
    code_obj* new_obj;
    std::cout <<"parsing while body on:"<< match_it->type <<"\n";
    match_it++;
        while (match_it < matches.end()){
            if (match_it->type == TokenKind::R_BR){
                match_it++;
                return;}
            new_obj = new code_obj(AstKind::stmt);
            parse_stmt(new_obj);
            while_body->set_child(new_obj);
        }
}
void parse_while_head   (code_obj* while_head){
    code_obj* new_obj;
    std::cout <<"parcing while head on :"<< match_it->type<<"\n";
    if (match_it->type != TokenKind::WHILE)
        return; // Синтаксическая ошибка
    match_it++;

    new_obj = new code_obj(AstKind::expr);
    parse_expr(new_obj);
    while_head->set_child(new_obj);

}
void parse_while        (code_obj* while_stmt){
    code_obj* new_obj;
    std::cout <<"parcing while on :"<< match_it->type<<"\n";
    new_obj = new code_obj(AstKind::while_head);
    parse_while_head(new_obj);
    while_stmt->set_child(new_obj);

    new_obj = new code_obj(AstKind::while_body);
    parse_while_body(new_obj);
    while_stmt->set_child(new_obj);
    //std::cout <<"finish parsing while body on:"<< match_it->type <<"\n";
}

void parse_else_body    (code_obj* else_body){
    code_obj* new_obj;
    std::cout <<"parcing else body on :"<< match_it->type<<"\n";
    match_it++;
    while (match_it < matches.end()){
        if (match_it->type == TokenKind::R_BR){
            match_it++;
            return;}
        new_obj = new code_obj(AstKind::stmt);
        parse_stmt(new_obj);
        else_body->set_child(new_obj);
    }

}


void parse_if_head      (code_obj* if_head){
    code_obj* new_obj;
    std::cout <<"parcing if head on :"<< match_it->type<<"\n";
    if (match_it->type != TokenKind::IF)
        return; // Синтаксическая ошибка
    match_it++;

    new_obj = new code_obj(AstKind::expr);
    parse_expr(new_obj);
    if_head->set_child(new_obj);
}
void parse_if_body      (code_obj* if_body){
    code_obj* new_obj;
    std::cout <<"parcing if body on :"<< match_it->type<<"\n";
    match_it++;
    while (match_it < matches.end()){
        if (match_it->type == TokenKind::R_BR){
            match_it++;
            return;}
        new_obj = new code_obj(AstKind::stmt);
        parse_stmt(new_obj);
        if_body->set_child(new_obj);
    }
}
void parse_if           (code_obj* if_stmt){
    code_obj* new_obj;
    std::cout <<"parcing if\n";
    new_obj = new code_obj(AstKind::if_head);
    parse_if_head(new_obj);
    if_stmt->set_child(new_obj);

    new_obj = new code_obj(AstKind::if_body);
    parse_if_body(new_obj);
    if_stmt->set_child(new_obj);

    if (match_it->type == TokenKind::ELSE){
        match_it++;
        new_obj = new code_obj(AstKind::else_body);
        parse_else_body(new_obj);
        if_stmt->set_child(new_obj);
    }
}


void parse_assign       (code_obj* assign_expr){
    code_obj* new_obj;
    std::cout <<"parcing assign on :"<< match_it->value.toStdString()<<"\n";;
    new_obj = new code_obj(AstKind::VAR, match_it->value);
    assign_expr->set_child(new_obj);
    match_it++;

    if (match_it == matches.end())
        return; // Синтаксическая ошибка
    if (match_it->type != TokenKind::ASSIGN_OP)
        return; // Синтаксическая ошибка
    new_obj = new code_obj(AstKind::ASSIGN_OP);
    assign_expr->set_child(new_obj);
    match_it++;

    if(match_it == matches.end())
        return; // Синтаксическая ошибка
    new_obj = new code_obj(AstKind::expr);
    parse_expr(new_obj);
    assign_expr->set_child(new_obj);
    match_it++;
    //std::cout <<"parsinf assign has completed on:"<< match_it->type <<"\n";
}

void parse_func      (code_obj* func){
    code_obj* new_obj;
    std::cout <<"parcing func:"<< match_it->value.toStdString()<<"\n";;
    match_it++;
    if (match_it == matches.end())
        return; // Синтаксическая ошибка
    if (match_it->type != TokenKind::L_RBR)
        return; // Синтаксическая ошибка
    match_it++;
    while ((match_it != matches.end()) && (match_it->type != TokenKind::R_RBR)) {
        switch (match_it->type) {
        case TokenKind::COMM:{}break;
        default:{
                new_obj = new code_obj(AstKind::value);
                parse_value(new_obj);
                func->set_child(new_obj);
            }
        }
        match_it++;
    }
    match_it++;
}

void parse_func_expr        (code_obj* func_expr){
    code_obj* new_obj;
    std::cout <<"parcing func_expr on :"<< match_it->value.toStdString()<<"\n";;
    new_obj = new code_obj(AstKind::VAR, match_it->value);
    func_expr->set_child(new_obj);
    match_it++;
    new_obj = new code_obj(AstKind::FUNC,match_it->value);
    parse_func(new_obj);
    func_expr->set_child(new_obj);
    match_it++;
}

void parse_assign_or_func   (code_obj* stmt){
    match_it++;
    code_obj* new_obj;
    std::cout <<"parcing stmt on :"<< match_it->type<<"\n";
    switch (match_it->type) {
    case TokenKind::ASSIGN_OP:{
        match_it--;
        new_obj = new code_obj(AstKind::assign_expr);
        parse_assign(new_obj);
        stmt->set_child(new_obj);
        return;
    }break;
    case TokenKind::FUNC:{
        match_it--;
        new_obj = new code_obj(AstKind::func_expr);
        parse_func_expr(new_obj);
        stmt->set_child(new_obj);
        return;
    }
    }
}

void parse_stmt         (code_obj* stmt){
    code_obj* new_obj;
    std::cout <<"parcing stmt on :"<< match_it->type<<"\n";
        switch (match_it->type) {
        case TokenKind::VAR:{
            parse_assign_or_func(stmt);
        }break;
        case TokenKind::WHILE:{
            new_obj = new code_obj(AstKind::while_expr);
            parse_while(new_obj);
            stmt->set_child(new_obj);
        }break;
        case TokenKind::IF:{
            new_obj = new code_obj(AstKind::if_expr);
            parse_if(new_obj);
            stmt->set_child(new_obj);
        }break;
        }
}


void parse_lang         (code_obj* lang){
    code_obj* new_obj;
    //std::cout <<"parcing lang\n";
    while (match_it < matches.end()){
        new_obj = new code_obj(AstKind::stmt);
        parse_stmt(new_obj);
        lang->set_child(new_obj);
    }
}


QVector<Operation> concate(QVector<Operation>first, QVector<Operation>second){
    for (const auto& el:second){
        first.push_back(el);
    }
    return first;
}


QVector<Operation> to_pnot(code_obj* root){
    QVector<Operation> pol = {};
    int anchor;
    switch (root->get_type()) {
    case AstKind::lang:{
        //std::cout <<"converting lang \n";
        for (const auto& ob:root->get_childs()){
            pol = concate(pol,to_pnot(ob)); // Конокотинация поизов
        }
        return pol;// [p1[0],...,p1[n],...,pk[0],...,pk[m]]
    }

    case AstKind::stmt:{
        //std::cout <<"converting stmt \n";
        return to_pnot(root->get_childs()[0]); // Просто возврощаем полиз
    }
    case AstKind::assign_expr:{
        //std::cout <<"converting assign_expr \n";
        pol=to_pnot(root->get_childs()[2]);// Полиз из выражения в присвоении
        pol.push_back({OpKind::ASSIGN_OP,"="}); // в конец полиза добавляем "="
        pol.push_front({OpKind::VAR, root->get_childs()[0]->get_value() }); // в начало полиза добавляем переменую
        return pol; //  [VAR,expr,=]
    }
    case AstKind::expr:{
        //std::cout <<"converting expr \n";
        if (root->get_childs()[0]->get_type()!=AstKind::new_expr){
            pol = concate(to_pnot(root->get_childs()[1]),to_pnot(root->get_childs()[2]));
            pol.push_back({OpKind::OP,root->get_childs()[0]->get_value()});
        }
        else {
            pol = to_pnot(root->get_childs()[0]);
        }
        return pol;// [expr1,expr2,OP]
    }
    case AstKind::value:{
        //std::cout <<"converting value \n";

        switch (root->get_childs()[0]->get_type()) {
        case AstKind::VAR:{
            pol.push_back({OpKind::VAR, root->get_childs()[0]->get_value()});
        }break;
        case AstKind::NUMB:{
            pol.push_back({OpKind::NUMB, root->get_childs()[0]->get_value()});
        }break;
        case AstKind::STR:{
            pol.push_back({OpKind::STR, root->get_childs()[0]->get_value()});
        }break;
        }
        return pol; //[VAR/NUMB]
    }
    case AstKind::while_expr:{
        //std::cout <<"converting while_expr \n";
        pol = to_pnot(root->get_childs()[0]); // Полиз условия
        anchor = pol.length();
        pol.push_back({OpKind::ADDR,""}); // Адрес перехода в конец(отн., пока не известен)
        pol.push_back({OpKind::FALSE_JMP,""});// Переход если условие False
        pol = concate(pol,to_pnot(root->get_childs()[1])); // Полиз тела цикла[head,ADDR,FALSE_JMP,body]
        pol.push_back({OpKind::ADDR,QString::number(-(pol.length()+2))}); // Адрес перехода в начало(отн.)
        pol.push_back({OpKind::JMP,""}); // Безусловный переход
        pol[anchor].value = QString::number(pol.length() - anchor - 1); // Установка адреса перехода в конец
        return pol; // [head,ADDR,FALSE_JMP, body, ADDR, !]
    }
    case AstKind::while_head:{
        //std::cout <<"converting while_head \n";
        return to_pnot(root->get_childs()[0]); // Просто полиз из expr
    }
    case AstKind::while_body:{
        //std::cout <<"converting while_body \n";
        for (const auto& ob:root->get_childs()){
            pol = concate(pol,to_pnot(ob)); // Конокотинация полизов
        }
        return pol;// [p1[0],...,p1[n],...,pk[0],...,pk[m]]
    }
    case AstKind::if_expr:{
        //std::cout <<"converting if_expr \n";
        pol = to_pnot(root->get_childs()[0]); // Полиз условия
        anchor = pol.length();
        pol.push_back({OpKind::ADDR,""}); // Адрес перехода в конец(отн., пока не известен)
        pol.push_back({OpKind::FALSE_JMP,""});// Переход если условие False
        pol = concate(pol,to_pnot(root->get_childs()[1])); // Полиз тела[head,ADDR,FALSE_JMP,body]
        pol[anchor].value = QString::number(pol.length() - anchor - 1); // Установка адреса перехода в конец
        return pol; // [head,ADDR,FALSE_JMP, body]
    }
    case AstKind::if_head:{
        //std::cout <<"converting if_head \n";
        return to_pnot(root->get_childs()[0]); // Просто полиз из expr
    }
    case AstKind::if_body:{
        //std::cout <<"converting if_body \n";
        for (const auto& ob:root->get_childs()){
            pol = concate(pol,to_pnot(ob)); // Конокотинация полизов
        }
        return pol;// [p1[0],...,p1[n],...,pk[0],...,pk[m]]
    }
    case AstKind::new_expr:{
       std::cout <<"converting new_expr \n";
       switch (root->get_childs()[0]->get_type()) {
       case AstKind::LIST:{
           pol.push_back({OpKind::FUNCTION, "new_list"});
       };break;
       case AstKind::TABLE:{
           pol.push_back({OpKind::FUNCTION, "new_table"});
       };break;
       }
       return pol;
    }
    case AstKind::func_expr:{
        //std::cout <<"converting assign_expr \n";
        pol=to_pnot(root->get_childs()[1]);// Полиз из выражения в присвоении
        pol.push_back({OpKind::FUNCTION,root->get_childs()[1]->get_value()}); // в конец полиза добавляем "="
        pol.push_front({OpKind::VAR, root->get_childs()[0]->get_value() }); // в начало полиза добавляем переменую
        return pol; //  [VAR,expr,=]
    }
    case AstKind::FUNC:{
        for(const auto& chld:root->get_childs()){
            pol = concate(pol,to_pnot(chld));
        }
        return pol;
    }
    }
}

void add_var(Operation* new_var){
    for (auto const& var:variables){
        if (var.name == new_var->value){
            return;
        }
    }
    variables.append(Variable(new_var->value));
}

Variable* search_var(QString name){
    for (int i = 0; i<variables.length(); i++){
        if (variables[i].name == name){
            return &variables[i];
        }
    }
}

Variable* operation_result(QString OP, Variable* first_arg, Variable* second_arg){
    if (OP == "+"){
        return new Variable(TypeKind::INT,first_arg->int_value + second_arg->int_value); // Добавить проверку типов
    }
    if (OP == "-"){
        return new Variable(TypeKind::INT,first_arg->int_value - second_arg->int_value);
    }
    if (OP == "*"){
        return new Variable(TypeKind::INT,first_arg->int_value * second_arg->int_value);
    }
    if (OP == "/"){
        return new Variable(TypeKind::INT,first_arg->int_value / second_arg->int_value);
    }
    if (OP == ">"){
        return new Variable(TypeKind::BOOL, first_arg->int_value > second_arg->int_value);
    }
    if (OP == "<"){
        return new Variable(TypeKind::BOOL, first_arg->int_value < second_arg->int_value);
    }
    /*if (OP == "=="){
        return new Variable(TypeKind::BOOL, first_arg->int_value == second_arg->int_value);
    }
    if (OP == ">="){
        return new Variable(TypeKind::BOOL, first_arg->int_value >= second_arg->int_value);
    }
    if (OP == "<="){
        return new Variable(TypeKind::BOOL, first_arg->int_value <= second_arg->int_value);
    }*/
};

Variable* func_get(Variable* var, Variable* list){
    if (list->type == TypeKind::LINKED_LIST){
        return list->list_value->get_data(var->int_value);
    }
    if (list->type == TypeKind::HASH_TABLE){
        return list->table_value->get_table(var);
    }
    return nullptr;
}

void func_append(Variable* var, Variable* list){
    if (list->type == TypeKind::LINKED_LIST){
        list->list_value->append_data(var);
    }
    return;
}

void func_add_pair(Variable* value, Variable* key, Variable* table){
    if (table->type == TypeKind::HASH_TABLE){table->table_value->insert(key,value);}
    return;
}

void assign_var(Variable* from_var, Variable* to_var){
    to_var->type = from_var->type;
    switch (to_var->type) {
    case TypeKind::INT:{
        to_var->int_value = from_var->int_value;
    }break;
    case TypeKind::FLOAT:{
        to_var->float_value = from_var->float_value;
    }break;
    case TypeKind::CHAR:{
        to_var->char_value = from_var->char_value;
    }break;
    case TypeKind::STRING:{
        to_var->str_value = from_var->str_value;
    }break;
    case TypeKind::BOOL:{
        to_var->bool_value = from_var->bool_value;
    }break;
    case TypeKind::ADDR:{
        to_var->addr_value = from_var->addr_value;
    }break;
    case TypeKind::LINKED_LIST:{
        to_var->list_value = from_var->list_value;
    }break;
    case TypeKind::HASH_TABLE:{
        to_var->table_value = from_var->table_value;
    }break;
    }

};

void compl_(){
    QVector<Operation>::iterator PC = poliz.begin();
    while (PC < poliz.end()){
        /*std::cout << "I am at: " << PC->type << " ("
            << PC->value.toStdString() << ") "<<stack.length()<<"\n";*/
        switch (PC->type) {
        case OpKind::VAR:{
            add_var(PC);
            stack.push(search_var(PC->value));
        }break;
        case OpKind::NUMB:{
            stack.push(new Variable(TypeKind::INT, PC->value.toInt()));
        }break;
        case OpKind::STR:{
            stack.push(new Variable(TypeKind::STRING, PC->value));
        }break;
        case OpKind::OP:{
            stack.push(operation_result(PC->value,stack.pop(),stack.pop()));
        }break;
        case OpKind::ASSIGN_OP:{
            assign_var(stack.pop(), stack.pop());
        }break;
        case OpKind::JMP:{
            int jmp = stack.pop()->addr_value;
            while (jmp != 0){
                if (jmp < 0){
                    jmp ++;
                    PC--;
                }
                else{
                    jmp--;
                    PC++;
                }
            }
        }break;
        case OpKind::FALSE_JMP:{
                int jmp = stack.pop()->addr_value;
                if (!stack.pop()->bool_value){
                    while (jmp != 0){
                        if (jmp < 0){
                            jmp ++;
                            PC--;
                        }
                        else{
                            jmp--;
                            PC++;
                        }
                    }
                }
        }break;
        case OpKind::ADDR:{
            stack.push(new Variable(TypeKind::ADDR, PC->value.toInt()));
        }break;
        case OpKind::FUNCTION:{
            if (PC->value == ".append"){
                func_append(stack.pop(), stack.pop());
            };
            if (PC->value == ".add_pair"){
                func_add_pair(stack.pop(),stack.pop(), stack.pop());
            };
            if (PC->value == ".get"){
                stack.push(func_get(stack.pop(),stack.pop()));
            };
            if (PC->value == "new_list"){
                stack.push(new Variable(TypeKind::LINKED_LIST, new LinkedList()));
            };
            if (PC->value == "new_table"){
                stack.push(new Variable(TypeKind::HASH_TABLE, new HashTable()));
            };
        }break;

        }
        PC++;
    }

};



int main(int argc, char *argv[])
{
        QString S;
        for (int i = 1; i < argc; i++)
        {
            S+=argv[i];
        }
        lexer(S);
        match_it = matches.begin();
        code_obj lang(AstKind::lang);
        parse_lang(&lang);
        //std::cout <<"parcing had complited \n";

        std::cout <<"\nPARSING RESULT:\n\n";
        lang.print_tree(0);

       std::cout <<"\nPNOTATION: \n\n";
        poliz = to_pnot(&lang);

       for (const auto& elm:poliz)
        {
            std::cout << elm.type << " ("
            << elm.value.toStdString() << ")" << std::endl;
        }


       compl_();


       std::cout <<"\nVARIABLES TABLE: \n\n";
       print_var();


        return 0;
}
